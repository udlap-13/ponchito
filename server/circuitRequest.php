<?php
$dbhost = "localhost";
$dbuser = "tester";
$dbpass = "strudel";
$dbname = "ponchito";

$con=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// Check connection
if (mysqli_connect_errno()){
  echo "error";
}

// Retrieve data from Query String
$ciudad1 = $_POST['ciudad1'];
$pais1 = $_POST['pais1'];
$ciudad2 = $_POST['ciudad2'];
$pais2 = $_POST['pais2'];
$identificador = $_POST['identificador'];

$query = "SELECT * FROM FechaCircuito NATURAL JOIN Circuito";


if($ciudad1 != -1 || $pais1!=-1 || $ciudad2!=-1 || $pais2!=-1 || $identificador!=-1){
  $query .= " WHERE ";
}
if($ciudad1 != -1){
  $query .= "ciudadSalida='$ciudad1'";
  if($pais1!=-1 || $ciudad2!=-1 || $pais2!=-1 || $identificador!=-1){
    $query .= " AND ";
  }
}
if($ciudad2 != -1){
  $query .= "ciudadLlegada='$ciudad2'";
  if($pais1!=-1 || $pais2!=-1 || $identificador!=-1){
    $query .= " AND ";
  }
}
if($pais1 != -1){
  $query .= "paisSalida='$pais1'";
  if($pais2!=-1 || $identificador!=-1){
    $query .= " AND ";
  }
}
if($pais2 != -1){
  $query .= "paisLlegada='$pais2'";
  if($identificador!=-1){
    $query .= " AND ";
  }
}
if($identificador != -1){
  $query .= "identificador='$identificador'";
}

$result = mysqli_query($con, $query);

//Build Result String
$display_string = '{"loader":[';
while($row =  mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    if ($display_string != '{"loader":[') {$display_string .= ",";}
    $display_string .= '{"identificador":"'  . $row["identificador"]     . '",';
    $display_string .= '"fechaSalida":"'   . $row["fechaSalida"]     . '",';
    $display_string .= '"nbPersonas":"'   . $row["nbPersonas"]     . '",';
    $display_string .= '"descripcionCircuito":"'   . $row["descripcionCircuito"]     . '",';
    $display_string .= '"ciudadSalida":"'  . $row["ciudadSalida"]    . '",';
    $display_string .= '"paisSalida":"'  . $row["paisSalida"]    . '",';
    $display_string .= '"ciudadLlegada":"'. $row["ciudadLlegada"]  . '",';
    $display_string .= '"paisLlegada":"'.     $row["paisLlegada"]  . '",';
    $display_string .= '"duracionCircuito":"'.     $row["duracionCircuito"]  . '",';
    $display_string .= '"precioCircuito":"'    . $row["precioCircuito"]      . '"}';
}
$display_string .="]}";

echo $display_string;

// Close connection
mysqli_close($con);
?>
