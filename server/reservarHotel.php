<?php
//Realizar reservacion del usuario de un circuito, crear el usuario si no existe o validar sus datos
//La reservacion ocurre solo si hay suficiente lugar para reservar
$dbhost = "localhost";
$dbuser = "tester";
$dbpass = "strudel";
$dbname = "ponchito";

$con=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// Check connection
if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

// Set autocommit to off
mysqli_autocommit($con, FALSE);


$usuario = $_POST['user'];
$pass = $_POST['pass'];
$tipo = $_POST['tipo'];
$numReservaciones = $_POST['numReservaciones'];
$nomHotel = $_POST['nomHotel'];
$ciudad = $_POST['ciudad'];
$pais = $_POST['pais'];

$query = "SELECT password FROM Cliente WHERE nombre='$usuario'";
$result1 = mysqli_query($con, $query);

$row = mysqli_fetch_array($result1, MYSQLI_ASSOC);
$p = $row[id];

//Obtener los datos de la simulacion
$query = "SELECT nombreHotel, ciudad, pais FROM Hotel WHERE nombreHotel='$nomHotel' AND ciudad='$ciudad' AND pais='$pais'";
$result4 = mysqli_query($con, $query);

if($result4 && mysqli_num_rows($result4)!=0){ //El hotel en donde reservar existe y la query se ejecuto
  if($result1 && mysqli_num_rows($result1)!=0 && ($pass == $p)){   //Si existe el usuario, y su contraseña es correcta
    //Actualizar su tipo
    $query = "UPDATE Cliente SET tipo='$tipo' WHERE nombre='$usuario'";
    $result2 = mysqli_query($con, $query);

    //Checar que hay suficientes habitaciones disponibles
    $query = "SELECT nombreHotel, ciudad, pais, (numCuartos-(COUNT(*)+'$numReservaciones')) as disponibles
              FROM Simulacion NATURAL JOIN FechaCircuito NATURAL JOIN Etapa NATURAL JOIN Hotel NATURAL JOIN ReservacionHotel
              WHERE numSimulacion = '$numSimulacion'
              GROUP BY nombreHotel, ciudad, pais, numCuartos";
    $result5 = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result5, MYSQLI_ASSOC);
    $disponibles = $row[disponibles];
    if($disponibles>=0){  //Crear reservacion si hay cuartos disponibles
      $query = "INSERT INTO ReservacionHotel (nombre, nombreHotel, ciudad, pais) VALUES ('$usuario', '$nomHotel', '$ciudad', '$pais')";
      $result3 = mysqli_query($con, $query);

      if($result2 && $result3){ //Usuario actualizado exitosamente y reservacion creada
        // Commit transaction
        mysqli_commit($con);
        echo "OK";
      }
      else{                     //actualizacion del usuario o de la reservacion fallo
        mysqli_rollback($con);
        echo "Reservation creation failed"; //Or wrong user type
      }
    }
    else{                   //No hay cuartos disponibles
      mysqli_rollback($con);
      echo "Not enough rooms";
    }
  }
  else{   //El usuario aun no existe
    //Crear usuario
    $query = "INSERT INTO Cliente (nombre, password, tipo) VALUES ('$usuario', '$pass', '$tipo')";
    $result2 = mysqli_query($con, $query);

    //Checar que hay suficientes habitaciones disponibles
    $query = "SELECT nombreHotel, ciudad, pais, (numCuartos-(COUNT(*)+'$numReservaciones')) as disponibles
              FROM Simulacion NATURAL JOIN FechaCircuito NATURAL JOIN Etapa NATURAL JOIN Hotel NATURAL JOIN ReservacionHotel
              WHERE numSimulacion = '$numSimulacion'
              GROUP BY nombreHotel, ciudad, pais, numCuartos";
    $result5 = mysqli_query($con, $query);
    $row = mysqli_fetch_array($result5, MYSQLI_ASSOC);
    $disponibles = $row[disponibles];
    if($disponibles>=0){  //Crear reservacion si hay cuartos disponibles
      $query = "INSERT INTO ReservacionHotel (nombre, nombreHotel, ciudad, pais) VALUES ('$usuario', '$nomHotel', '$ciudad', '$pais')";
      $result3 = mysqli_query($con, $query);

      if($result2 && $result3){ //Usuario creado exitosamente y reservacion creada
        // Commit transaction
        mysqli_commit($con);
        echo "OK";
      }
      else{                     //Fallo la creacion del usuario o de la reservacion
        mysqli_rollback($con);
        echo "User or reservation creation failed";
      }
    }
    else{                       //No hay cuartos disponibles
      mysqli_rollback($con);
      echo "Not enough rooms";
    }
  }
}
else{ //El hotel no existe
  mysqli_rollback($con);
  echo "There's no such Hotel";
}

// Close connection
mysqli_close($con);
?>
