<?php
$dbhost = "localhost";
$dbuser = "tester";
$dbpass = "strudel";
$dbname = "ponchito";

$con=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// Check connection
if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$query = "SELECT * FROM Ciudad";
$result = mysqli_query($con, $query);

//Build Result String
$display_string = '{"loader":[';
while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    if ($display_string != '{"loader":[') {$display_string .= ",";}
    $display_string .= '{"ciudad":"'  . $row["ciudad"]     . '",';
    $display_string .= '"pais":"'    . $row["pais"]      . '"}';
}
$display_string .="]}";

echo $display_string;

// Close connection
mysqli_close($con);
?>
