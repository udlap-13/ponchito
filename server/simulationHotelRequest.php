<?php
$dbhost = "localhost";
$dbuser = "tester";
$dbpass = "strudel";
$dbname = "ponchito";

$con=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// Check connection
if (mysqli_connect_errno()){
  echo "error";
}

// Retrieve data from Query String
$numSimulacion = $_POST['numSimulacion'];

$query = "SELECT nombreHotel, ciudad, pais, direccionHotel, numCuartos, precioCuarto, precioDesayuno
          FROM Simulacion NATURAL JOIN FechaCircuito NATURAL JOIN Etapa NATURAL JOIN Hotel
          WHERE numSimulacion = '$numSimulacion'";
$result = mysqli_query($con, $query);

//Build Result String
$display_string = '{"loader":[';
while($row =  mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    if ($display_string != '{"loader":[') {$display_string .= ",";}
    $display_string .= '{"nombreHotel":"'  . $row["nombreHotel"]     . '",';
    $display_string .= '"ciudad":"'   . $row["ciudad"]     . '",';
    $display_string .= '"pais":"'   . $row["pais"]     . '",';
    $display_string .= '"direccionHotel":"'   . $row["direccionHotel"]     . '",';
    $display_string .= '"numCuartos":"'  . $row["numCuartos"]    . '",';
    $display_string .= '"precioCuarto":"'  . $row["precioCuarto"]    . '",';
    $display_string .= '"precioDesayuno":"'    . $row["precioDesayuno"]      . '"}';
}
$display_string .="]}";

echo $display_string;

// Close connection
mysqli_close($con);
?>
