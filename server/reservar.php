<?php
//Realizar reservacion del usuario de un circuito, crear el usuario si no existe o validar sus datos
//La reservacion ocurre solo si hay suficiente lugar para reservar
$dbhost = "localhost";
$dbuser = "tester";
$dbpass = "strudel";
$dbname = "ponchito";

$con=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// Check connection
if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

// Set autocommit to off
mysqli_autocommit($con, FALSE);


$usuario = $_POST['user'];
$pass = $_POST['pass'];
$tipo = $_POST['tipo'];
$numReservaciones = $_POST['numReservaciones'];
$numSimulacion = $_POST['numSimulacion'];

$query = "SELECT password FROM Cliente WHERE nombre='$usuario'";
$result1 = mysqli_query($con, $query);

$row = mysqli_fetch_array($result1, MYSQLI_ASSOC);
$p = $row[id];

//Obtener los datos de la simulacion
$query = "SELECT identificador, fechaSalida FROM Simulacion WHERE numSimulacion='$numSimulacion'";
$result4 = mysqli_query($con, $query);
$row1 = mysqli_fetch_array($result4, MYSQLI_ASSOC);
$identificador = $row1[identificador];
$fechaSalida = $row1[fechaSalida];

if($result1 && ($pass == $p)){   //Si existe el usuario, y su contraseña es correcta
  //Actualizar su tipo
  $query = "UPDATE Cliente SET tipo='$tipo' WHERE nombre='$usuario'";
  $result2 = mysqli_query($con, $query);

  //Checar que hay suficientes lugares disponibles en el circuito
  $query = "SELECT nombre, identificador, fechaSalida, (nbPersonas-(COUNT(*)+'$numReservaciones')) as disponibles
            FROM Simulacion NATURAL JOIN FechaCircuito NATURAL JOIN Reservacion
            WHERE numSimulacion = '$numSimulacion'
            GROUP BY nombre, identificador, fechaSalida, nbPersonas";
  $result5 = mysqli_query($con, $query);
  $row = mysqli_fetch_array($result5, MYSQLI_ASSOC);
  $disponibles = $row[disponibles];
  if($disponibles>=0){  //Crear reservacion si hay lugares disponibles
    //Crear reservacion
    $query = "INSERT INTO Reservacion (nombre, identificador, fechaSalida) VALUES ('$usuario', '$identificador', '$fechaSalida')";
    $result3 = mysqli_query($con, $query);

    if($result2 && $result3){ //Usuario actualizado exitosamente y reservacion creada
      // Commit transaction
      mysqli_commit($con);
      echo "OK";
    }
    else{                     //Fallo la actualizacion del usuario o de la reservacion
      mysqli_rollback($con);
      echo "Reservation creation failed"; //Or wrong user type
    }
  }
  else{                       //No hay lugares disponibles
    mysqli_rollback($con);
    echo "Not enough free slots";
  }
}

else{   //El usuario aun no existe
  //Crear usuario
  $query = "INSERT INTO Cliente (nombre, password, tipo) VALUES ('$usuario', '$pass', '$tipo')";
  $result2 = mysqli_query($con, $query);

  //Checar que hay suficientes lugares disponibles en el circuito
  $query = "SELECT nombre, identificador, fechaSalida, (nbPersonas-(COUNT(*)+'$numReservaciones')) as disponibles
            FROM Simulacion NATURAL JOIN FechaCircuito NATURAL JOIN Reservacion
            WHERE numSimulacion = '$numSimulacion'
            GROUP BY nombre, identificador, fechaSalida, nbPersonas";
  $result5 = mysqli_query($con, $query);
  $row = mysqli_fetch_array($result5, MYSQLI_ASSOC);
  $disponibles = $row[disponibles];
  if($disponibles>=0){  //Crear reservacion si hay lugares disponibles
    //Crear reservacion
    $query = "INSERT INTO Reservacion (nombre, identificador, fechaSalida) VALUES ('$usuario', '$identificador', '$fechaSalida')";
    $result3 = mysqli_query($con, $query);

    if($result2 && $result3){ //Usuario creado exitosamente y reservacion creada
      // Commit transaction
      mysqli_commit($con);
      echo "OK";
    }
    else{                     //Fallo la creacion del usuario o de la reservacion
      mysqli_rollback($con);
      echo "User or reservation creation failed";
    }
  }
  else{                       //No hay lugares disponibles
    mysqli_rollback($con);
    echo "Not enough free slots";
  }
}

// Close connection
mysqli_close($con);
?>
