///<reference path="jquery-2.1.4.min.js" />
function initUserFormListeners()
{
    //TESTING ONLY
    /*sessionStorage.setItem("usuario", "Tester User");
    sessionStorage.setItem("contrase�a", "abc123");
    sessionStorage.setItem("tipo", "65 gigawatts");
    sessionStorage.setItem("numReservaciones", "1");
    sessionStorage.setItem("numSimulacion", "9001");            //Its over 9000!
    sessionStorage.setItem("nomHotel", "Hotel TEST");
    sessionStorage.setItem("ciudad", "Pruebalandia");
    sessionStorage.setItem("pais", "El imperio de pruebalandia");*/
    //Getting the target containers
    var passContain = $('[name="password"]');                   //The input that contains the password
    var reseContain = $('[name="num_reservations"]');           //The input that contains the number of reservations
    var submitter = $('[name="reservation_submit"]');           //The input that contains the submit button
    //Retrieving session data from local storage
    var usuario = localStorage.getItem("usuario");
    var contrasena = sessionStorage.getItem("contrase�a");
    var tipo = sessionStorage.getItem("tipo");
    var numReservaciones = sessionStorage.getItem("numReservaciones");
    var numSimulacion = sessionStorage.getItem("numSimulacion");
    var nomHotel = sessionStorage.getItem("nomHotel");
    var ciudad = sessionStorage.getItem("ciudad");
    var pais = sessionStorage.getItem("pais");

    //Enclosure to create submit button listener.
    submitter.click(function (event) {
        //Code executed when submit is triggered:-------------
        submitReservation();
        //----------------------------------------------------
        event.preventDefault();
    });

    var submitReservation = function()
    {
        var dataString = 'user=' + usuario +        //Cambio de nombres por agus
                '&pass=' + contrase�a +
                '&tipo=' + tipo +
                '&numReservaciones=' + numReservaciones +
                '&numSimulacion=' + numSimulacion;
        //Posting call 10
        $.ajax(
            {
                type: 'POST',
                url: 'server/reservar.php',
                data: dataString,
                success: function (answer) {
                    if (answer == -1) {
                        alert("Actividad inv�lida");
                    }
                },
                error: function () {
                    alert('Something went wrong, ERROR');
                    console.log(dataString);
                }
            });
    }

}
