///<reference path="jquery-2.1.4.min.js" />

function initGlobalFeatures()       //Like the sidebar
{
    //Sidebar container
    var sidebar = $('#sidebar');
    //Getting tabs
    var tab1 = $('#tab-1');
    var tab2 = $('#tab-2');
    var tab3 = $('#tab-3');
    //Adding listeners-----------------------------------
    tab1.change(function (event) {
        //Code executed when submit is triggered
        tab1Transform();
        event.preventDefault();
    });
    tab2.change(function (event) {
        //Code executed when submit is triggered
        tab2Transform();
        event.preventDefault();
    });
    tab3.change(function (event) {
        //Code executed when submit is triggered
        tab3Transform();
        event.preventDefault();
    });
    //Behavior changing of the sidebar---------------------
    var tab1Transform = function()
    {
        //Emptying sidebar
        sidebar.empty();
        //Emptying tables
        $('#Folleto').empty();
        //Reloading the simulations
        initSimulacionListener()
    }
    var tab2Transform = function () {
        //Emptying sidebar
        sidebar.empty();
        //Emptying tables
        $('#Folleto').empty();
        //Reloading the simulations
        initSimulacionListener()
    }
    var tab3Transform = function () {       //Folleto
        //Emptying sidebar
        sidebar.empty();
        //Filling sidebar
        initFolletoListeners();
        //Reloading the simulations
        initSimulacionListener()
    }
}