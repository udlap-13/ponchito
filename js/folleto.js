///<reference path="jquery-2.1.4.min.js" />
//Pseudo permanent data
var ciudad1 = -1;    //Salida
var pais1 = -1;
var ciudad2 = -1;    //Llegada
var pais2 = -1;
var identificador = -1;
var simulationCart = [];


function initFolletoListeners()
{
    //Clearing sidebar:
    $('#sidebar').empty();

    //Necessary data:
    var cities = [];            //It will hold cityData objects.

    //Update sidebar:
    this.updateFolletoSidebar = function()
    {
        //Preparing data string
        var dataStringNull = 'ciudad1=' + -1 +
                '&pais1=' + -1 +
                '&ciudad2=' + -1 +
                '&pais2=' + -1 +
                '&identificador=' + -1;

        var dataStringSpecial = 'ciudad1=' + ciudad1 +
                '&pais1=' + pais1 +
                '&ciudad2=' + ciudad2 +
                '&pais2=' + pais2 +
                '&identificador=' + identificador;
        //Ajax call
        $.ajax(             //This answers clauses 1 and 2
            {
                type: 'POST',
                url: 'server/circuitRequest.php',
                data: dataStringNull,
                success: function (answer) {
                    if (answer == -1) {
                        alert("Actividad inv�lida");
                    }
                    console.log(answer);
                    var sidebar = $('#sidebar');
                    //Parsing json
                    var receptor = $.parseJSON(answer);
                    var receptor = receptor.loader;
                    console.log(receptor);
                    //Preparing entry string:
                    var titleString1 = '<h1>Ciudades Salida</h1>'
                    sidebar.append(titleString1);
                    for(var i = 0; i < receptor.length; i++)
                    {
                        var salidaString =
                        '<tr><td><input type="radio" name="sa-' + i + '" value="select"></td>' +
						'<td>'+ receptor[i].paisSalida + ', '+ receptor[i].ciudadSalida+'</td></tr>'
                        sidebar.append(salidaString);
                        var actString = 'sa-' + i;
                        //Enclosing to make sure it runs immediately
                        (function (address, holder) {
                            var targ = $('[name="'+address+'"]');
                            targ.change(function () {
                                if (targ.is(':checked')) {
                                    ciudad1 = holder.ciudadSalida;
                                    pais1 = holder.paisSalida;
                                    console.log(ciudad1 + ', ' + pais1);
                                    modifyFolletoTable();
                                }
                                else {
                                    // Checkbox is not checked. Therefore do nothing
                                }
                            });
                        })(actString, receptor[i]);         //Passing parameters to make sure they save loop values
                    }
                    var titleString2 = '<h1>Circuitos</h1>'
                    sidebar.append(titleString2);
                    for (var i = 0; i < receptor.length; i++) {
                        var salidaString =
                        '<tr><td><input type="radio" name="ci-' + i + '" value="select"></td>' +
						'<td>' + receptor[i].identificador + '</td></tr>'
                        sidebar.append(salidaString);
                        var actString = 'ci-' + i;
                        //Enclosing to make sure it runs immediately
                        (function (address, holder) {
                            var targ = $('[name="' + address + '"]');
                            targ.change(function () {
                                if (targ.is(':checked')) {
                                    identificador = holder.identificador;
                                    console.log(identificador);
                                    modifyFolletoTable();
                                }
                                else {
                                    // Checkbox is not checked. Therefore do nothing
                                }
                            });
                        })(actString, receptor[i]);         //Passing parameters to make sure they save loop values
                    }
                    var titleString3 = '<h1>Ciudades Llegada</h1>'
                    sidebar.append(titleString3);
                    for (var i = 0; i < receptor.length; i++) {
                        var salidaString =
                        '<tr><td><input type="radio" name="le-' + i + '" value="select"></td>' +
						'<td>' + receptor[i].paisLlegada + ', ' + receptor[i].ciudadLlegada + '</td></tr>'
                        sidebar.append(salidaString);
                        var actString = 'le-' + i;
                        //Enclosing to make sure it runs immediately
                        (function (address, holder) {
                            var targ = $('[name="' + address + '"]');
                            targ.change(function () {
                                if (targ.is(':checked')) {
                                    ciudad2 = holder.ciudadLlegada;
                                    pais2 = holder.paisLlegada;
                                    console.log(ciudad2 + ', ' + pais2);
                                    modifyFolletoTable();
                                }
                                else {
                                    // Checkbox is not checked. Therefore do nothing
                                }
                            });
                        })(actString, receptor[i]);         //Passing parameters to make sure they save loop values
                    }
                },
                error: function (answer) {
                    alert('Something went wrong, ERROR');
                    console.log(answer)
                    console.log(dataStringNull);
                }
            });
        //Claue 3 is made by JS
        //Clause 4 is the same as clause 1 but with our expanded parameters.
        $.ajax(             //This answers clauses 1 and 2
            {
                type: 'POST',
                url: 'server/circuitRequest.php',
                data: dataStringSpecial,
                success: function (answer) {
                    if (answer == -1) {
                        alert("Actividad inv�lida");
                    }
                    console.log(answer);
                    //Success code.
                    var target = $('#Folleto');
                    console.log(target);
                    var receptor = $.parseJSON(answer);
                    var receptor = receptor.loader;
                    console.log(receptor);
                    for (var i = 0; i < receptor.length; i++)
                    {
                        var subject = receptor[i];
                        var subString = '<tr>' +
										'<td>' + subject.identificador + '</td>' +
										'<td>' + subject.fechaSalida + '</td>' +
										'<td>' + subject.ciudadSalida + ', ' + subject.paisSalida + '</td>' +
                                        '<td>' + subject.ciudadLlegada + ', ' + subject.paisLlegada + '</td>' +
										'<td><button class="editbtn" id="editbtn-'+ i +'">simular</button>' +
										'<br># disponibles </td>' + 
										'</tr>';
                        target.append(subString);
                        var actString = 'editbtn-' + i;
                        //Enclosing to make sure it runs immediately
                        /*
                        Storing the important information from the current selection to permit future manipulation
                        of simulations.
                        */
                        (function (address, holder) {
                            var targ = $('#' + address);
                            targ.click(function (event) {
                                var simulationPack = new simulationSeed(holder.identificador, holder.fechaSalida);
                                var proceed = true;     //For avoiding duplicates
                                for (var j = 0; j < simulationCart.length; j++) //Avoiding duplicates
                                {
                                    if(simulationCart[j].identificador == simulationPack.identificador)
                                    {
                                        proceed = false;
                                        break;
                                    }
                                }
                                if(proceed == true)
                                {
                                    simulationCart.push(simulationPack);
                                    console.log(simulationCart);
                                    event.preventDefault();
                                }
                            });
                        })(actString, subject);         //Passing parameters to make sure they save loop values
                    }
                    
                },
                error: function (answer) {
                    alert('Something went wrong, ERROR');
                    console.log(answer);
                    console.log(dataStringNull);
                }
            });

        //Updating city information
        $.ajax(             //This answers clauses 1 and 2
            {
                type: 'POST',
                url: 'server/cityRequest.php',
                success: function (answer) {
                    if (answer == -1) {
                        alert("Actividad inv�lida");
                    }
                    console.log(answer);
                },
                error: function (answer) {
                    alert('Something went wrong, ERROR');
                    console.log(answer)
                }
            });
    }
    var submitSimulation = function (nombreLugar, ciudad, pais, direccionLugar, descripcionLugar, precioLugar)
        //6.1
    {
        var dataStringSimulation = 'numbreLugar=' + nombreLugar +
                '&ciudad=' + ciudad +
                '&pais=' + pais +
                '&direccionLugar=' + direccionLugar +
                '&descripcionLugar=' + descripcionLugar+
                '&precioLugar=' + precioLugar;
        //Ajax
        $.ajax(             //This answers clauses 1 and 2
            {
                type: 'POST',
                url: 'server/simulationPlaceRequest.php',
                data: dataStringSimulation,
                success: function (answer) {
                    if (answer == -1) {
                        alert("Actividad inv�lida");
                    }
                },
                error: function () {
                    alert('Something went wrong, ERROR');
                    console.log(dataStringSimulation);
                }
            });
    }
    this.updateFolletoSidebar();
    console.log('success');
}

//Live modification of folleto table
function modifyFolletoTable()
{
    var dataStringSpecial = 'ciudad1=' + ciudad1 +
                '&pais1=' + pais1 +
                '&ciudad2=' + ciudad2 +
                '&pais2=' + pais2 +
                '&identificador=' + identificador;
    $.ajax(             //This answers clauses 1 and 2
            {
                type: 'POST',
                url: 'server/circuitRequest.php',
                data: dataStringSpecial,
                success: function (answer) {
                    if (answer == -1) {
                        alert("Actividad inv�lida");
                    }
                    console.log(answer);
                    //Success code.
                    var target = $('#Folleto');
                    target.empty();
                    console.log(target);
                    var receptor = $.parseJSON(answer);
                    var receptor = receptor.loader;
                    console.log(receptor);
                    for (var i = 0; i < receptor.length; i++) {
                        var subject = receptor[i];
                        var subString = '<tr>' +
										'<td>' + subject.identificador + '</td>' +
										'<td>' + subject.fechaSalida + '</td>' +
										'<td>' + subject.ciudadSalida + ', ' + subject.paisSalida + '</td>' +
                                        '<td>' + subject.ciudadLlegada + ', ' + subject.paisLlegada + '</td>' +
										'<td><button class="editbtn" id=editbtn-"' + i + '">reservar</button>' +
										'<br># disponibles </td>' +
										'</tr>';
                        target.append(subString);
                        console.log('iteration');
                    }

                },
                error: function (answer) {
                    alert('Something went wrong, ERROR');
                    console.log(answer);
                    console.log(dataStringNull);
                }
            });
}


//Storage objects
function cityData(ciudad, pais)
{
    this.ciudad = ciudad;
    this.pais = pais;
}
function simulationSeed(iden, fech)
{
    this.identificador = iden;
    this.fechaSalida = fech;
}